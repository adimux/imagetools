from setuptools import setup

setup(name="imagetools",
      version="0.1",
      description="Image Processing Tools",
      url="http://github.com/adimux/imagetools",
      author="Adam Cherti",
      author_email="adam@cherti.name",
      license="MIT",
      packages=['imagetools'],
      zip_safe=False)
