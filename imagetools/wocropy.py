"""
Wrapper of ocropus scripts
"""

import os
from os import listdir
from os.path import isfile, join, isdir
import sys
from imagetools.utils import file_without_extension


class Wocropy(object):
    """
    Wrapper for ocropy.
    Provides functions to call ocropy scripts in order to perform binarization,
        segmentation, prediction, hocr and various other operations.

    Pre-conditions : a valid installation of ocropy should exist in the system
        In the other case, you can pull it from https://github.com/tmbdev/ocropy
        it in this script folder or anywhere else.
    """
    OCROPY = "ocropy"
    TESSERACT = "tesseract"
    PREDICTION_METHODS = {
        OCROPY: "Ocropy",
        TESSERACT: "Tesseract",
    }
    DEFAULT_LANG = "eng"

    def __init__(self, ocropy_dir=join(os.path.realpath(os.path.dirname(__file__)), "ocropy")):
        """
        Inits ocropy wrapper

        Parameters
        ---------
        ocropy_dir : string
            A directory path pointing to ocropy scripts
        """
        self.ocropy_dir = os.path.abspath(ocropy_dir)

        self.tesseract_prediction_models = {
            "eng":"eng",
            "fra":"fra"
        }
        self.ocropy_prediction_models = {
            "eng":self.__get_ocropy_file("models", "en-default.pyrnn.gz"),
            "fra":os.path.abspath("french-receipts-from-en-00129000.pyrnn.gz"),

            #"fra":os.path.abspath("french-receipts-00049000.pyrnn.gz"),
        }
        self.default_lang = "eng"

    def set_ocropy_dir(self, dirpath):
        self.ocropy_dir = os.path.abspath(dirpath)

    def get_ocropy_dir(self):
        return self.ocropy_dir

    def __has_prediction_model(self, lang):
        """
        Assests that the ocropy prediction model for this language exists
        """
        return lang in self.ocropy_prediction_models

    def __has_tesseract_prediction_model(self, lang):
        """
        Assests that the tesseract prediction data for this language exists
        """
        languages = lang.split("+")
        exists = True
        for lang in languages:
            exists = exists and (lang in self.tesseract_prediction_models)
        return exists

    def __get_prediction_model_path(self, lang):
        """
        Returns the prediction model path corresponding to the following language

        Parameters
        ----------
        lang : string
            Language

        throws AssertionError if the language is not supported
        """
        assert self.__has_prediction_model(lang)
        return self.ocropy_prediction_models[lang]

    def __get_tesseract_prediction_model_name(self, lang):
        """
        Returns the prediction model name corresponding to the following language 
            for Tesseract

        Parameters
        ----------
        lang : string
            language

        throws AssertionError if the language is not supported
        """
        assert self.__has_tesseract_prediction_model(lang), "Prediction model for %f language for Tesseract does not exist"
        languages = lang.split("+")
        langs = []
        for lang in languages:
            langs.append(self.tesseract_prediction_models[lang])
        
        return "+".join(langs)

    @property
    def binarization_script(self):
        """
        Binarization script call command
        """
        return self.__get_ocropy_file("ocropus-nlbin")
    @property
    def segmentation_script(self):
        """
        Segmentation script call command
        """
        return self.__get_ocropy_file("ocropus-gpageseg")
    @property
    def prediction_script(self):
        """
        Prediction script call command.
        """
        return self.__get_ocropy_file("ocropus-rpred")
    @property
    def tesseract_script(self):
        """
        Tesseract script call command.
        """
        return "tesseract"
    @property
    def tesseract_prediction_script(self):
        """
        Tesseract batch prediction script call command.
        """
        return os.path.abspath("tesseract-rpred.py")
    @property
    def hocr_script(self):
        """
        Ocropy HOCR script call command.
        """
        return self.__get_ocropy_file("ocropus-hocr")
    @property
    def hocr_to_text_script(self):
        """
        Hocr to text converter script call command.
        """
        return os.path.abspath(os.path.join(os.path.dirname(__file__), "extract_ocropy_text.py"))

    def __get_ocropy_file(self, *path):
        """
        Generates an absolute path to a file in the ocropy
        base directory.
        
        Parameters
        ---------
        path : tuple
            a tuple of path segments
        
        Pre-conditions : self.ocropy_dir should be a path
            pointing to a valid ocropy installation
        """
        result = self.ocropy_dir
        
        for sub in path:
             result = join(result, sub)
        return result

    def __has_already_binarized(self, image_path, image_dir=None):
        if image_dir is None:
            image_dir = self.__get_image_dir_from_path(image_path)
        return os.path.exists(join(image_dir, "0001.bin.png"))

    def binarize(self, image_path, output_dir=None, force=False):
        """
        Performs binarization on an image

        Pre-conditions : image_path must refer to a valid image. Output dir, if provided,
            must refer to a valid directory path that may exist already (or not)
        Post-conditions : the output directror

        Parameters
        ----------
        image_path : string
            path to the image file
        output_dir : string | None
            path to output directory or None. If None is provided, the directory will have
            the same name as the image, but without the file extension.

        Return
        ------
        output_dir : string
            image dir
        """
        image_name = file_without_extension(os.path.basename(image_path))

        if self.__has_already_binarized(image_path) and not force:
            print "Image already binarized... Skip binarization"
            return self.__get_image_dir_from_path(image_path)

        if output_dir is None:
            output_dir = join(os.path.dirname(image_path), image_name)

        binarization_command = "python \"%s\" -n \"%s\" -o \"%s\"" % \
            (self.binarization_script, image_path, output_dir)

        print "Perform Binarization"
        print binarization_command

        os.system(binarization_command)

        return os.path.abspath(output_dir)

    def __has_already_segmented(self, image_path=None, image_dir=None):
        if image_dir is None:
            if image_path is not None:
                image_dir = self.__get_image_dir_from_path(image_path)
            else:
                raise ValueError("image_file must be provided if image_dir has been not")

        segments_dir = join(image_dir, "0001")

        if not isdir(segments_dir):
            return False

        return len(listdir(segments_dir)) > 0

    def segment(self, image_dir, force=False):
        """
        Performs segmentation using ocropy segmentation script when we are given a directory
        generated by the binarization of an image

        Pre-conditions : the directory must exist and contain a file named "0001.bin.png"
            representing the binary image
        Post-conditions : the command executed will create a directory inside the one provided
            that has the same name as the image but without the file extension. The directory
            will contain the segments of texts detected in the binarized image
    
        Parameters
        ----------
        image_dir : string
            directory generated by the binarization of an image

        Return
        ------
        """
        assert os.path.exists(image_dir), "Segmentation error : Image dir %s does not exist." % (image_dir)
        assert os.path.exists(join(image_dir, "0001.bin.png"))
        
        if self.__has_already_segmented(image_dir=image_dir) and not force:
            print "Segmentation already performed... Skip segmentation"
            return

        segmentation_command = "python \"%s\" \"%s\" -n --minscale 9 --maxcolseps 0 " % (
                self.segmentation_script,
                join(image_dir, "0001.bin.png")
            )
        
        print "Perform segmentation"
        print segmentation_command
        os.system(segmentation_command)

    def __get_image_dir_from_path(self, image_path):
        """
        Returns the corresponding processing dir to the image
        
        Parameters
        ---------
        image_path : string
            path to the image
        """
        return os.path.abspath(file_without_extension(image_path))

    def __has_already_predicted(self, image_path=None, image_dir=None):
        if image_dir is None:
            if image_path is not None:
                image_dir = self.__get_image_dir_from_path(image_path)
            else:
                raise ValueError("image_file must be provided since image_dir has been not")
        
        segments_dir = join(image_dir, "0001")

        if not isdir(segments_dir):
            return False

        nb_segments = 0
        nb_predicted = 0
        for filename in listdir(segments_dir):
            if filename.lower().endswith(".bin.png"):
                if os.path.exists(
                        join(
                            segments_dir,
                            file_without_extension(filename, ".bin.png") + ".txt"
                        )
                    ):
                    nb_predicted = nb_predicted + 1
                nb_segments = nb_segments + 1
        return nb_predicted == nb_segments

    def predict(self, image_dir, method=OCROPY, lang=DEFAULT_LANG, force=False):
        """
        Predict contents of segments images inside a directory generated by the 
        binarization and segmentation of an image.

        Pre-conditions :
        - The provided directory must exist and must contain a directory 0001 containing
            png files which represent the segments to predict
        - The prediciton method must be listed in Wocropy.SEGMENTATION_METHODS

        Post-condtions :
        - Generates text files containing the predictions aside to the image files

        Parameters
        ----------
        image_dir : string
            directory generated by the binarization of an image
        lang : string
            prediction language
        """
        assert method in self.__class__.PREDICTION_METHODS, "Prediction " \
            + "method %s is not supported" % method
        assert os.path.exists(image_dir), "The directory " \
            + "you provided %s does not exist" % image_dir
        assert os.path.exists(join(image_dir, "0001")), "Segments " \
            + "directory %s not found" % join(image_dir, "0001")

        if self.__has_already_predicted(image_dir=image_dir) and not force:
            print "Prediction already performed.. Skip prediction"
            return

        image_dir = os.path.abspath(image_dir)

        if method is self.__class__.OCROPY:
            """
            Predict using ocropy prediction script
            """

            if not self.__has_prediction_model(lang):
                lang = self.default_lang

            prediction_command = "python \"%s\" -m \"%s\" \"%s\" " % (
                self.prediction_script,
                # path to prediction script
                self.__get_prediction_model_path(lang),
                # path to corresponding prediction model file
                join(join(image_dir, "0001"), "*.png")
                # Target all png files inside the 0001 directory which represents the segments
            )
            print "Perform prediction using %s" % self.__class__.PREDICTION_METHODS[method]
            print prediction_command
            os.system(prediction_command)

        elif method is self.__class__.TESSERACT:
            """
            Predict using Tesseract
            """

            segments_dir = join(image_dir, "0001")
            # List segment files
            segments_files = []
            for filename in listdir(segments_dir):
                if filename.lower().endswith(".png"):
                    segments_files.append(filename)

            # Loop over them and recognize the text using Tesseract
            for segment_file in segments_files:
                segment_file_path = join(segments_dir, segment_file)

                prediction_file = join(
                    segments_dir,
                    file_without_extension(
                        segment_file,
                        extension=".bin.png"
                    )
                )
                prediction_command = "\"%s\" \"%s\" \"%s\" -l \"%s\" " % (
                    self.tesseract_script,
                    segment_file_path,
                    # Path to the segment image
                    prediction_file,
                    # Path to the corresponding prediction text file
                    self.__get_tesseract_prediction_model_name(lang)
                    # Tesseract language option
                )

                print "Perform prediction using %s" % self.__class__.PREDICTION_METHODS[method]
                print "Segment file -- %s.." % segment_file
                print prediction_command
                os.system(prediction_command)
            print ""
            print "All predictions done"

    """
    def ocropy_train(self, image_dir):
        segments_dir = join(image_dir, "0001")
        segments_dir
    """
    def perform_hocr(self, image_dir, label=''):
        """
        Performs hocr : convert image segments to an html file
            with the texts and the correct layouts

        Pre-condtitions : binarizationm segmentation and prediction must have
            been performed on an image and the image_dir which was the output
            of those steps should exist.

        Parameters
        ----------
        image_dir ; string
            directory generated by the binarization
        """
        html_file_path = join(image_dir, "prediction-%s.html" % label)
        hocr_command = "python %s \"%s\" -o \"%s\"" % (
            self.hocr_script,
            join(image_dir, "0001.bin.png"),
            html_file_path,
        )

        print "Perform HOCR (recombine segments to form the html file)"
        print hocr_command
        os.system(hocr_command)

        return html_file_path

    def hocr_to_text(self, html_file):
        """
        Converts HOCR html file into text. It compares
        blocks of texts' positions in the html file
        and generates a line by line text

        Pre-condtions: html_file must a valid path to an
            existing html file

        Parameters
        ----------
        html_file : string
            path to the hocr html file
        """
        conversion_command = "python \"%s\" \"%s\"" % (
            self.hocr_to_text_script,
            html_file
        )
        print "Perform HOCR html file conversion to text file"
        print conversion_command

        out = os.popen(conversion_command).read()
        err = None
        """
        proc = subprocess.Popen(
            [
                self.hocr_to_text_script,
                "html_file"
            ],
            stdout=subprocess.PIPE,
            shell=True)
        (out, err) = proc.communicate()
        """
        assert not err, "An error occured while performing the " \
            + "conversion from html (hocr) to text : %s" % err

        #line_by_line_text = os.system(conversion_command)
        line_by_line_text = out

        return line_by_line_text

    def scan(self, image_path, use_tesseract_prediction=True, lang=DEFAULT_LANG, force_prediction=False):
        """
        Returns HTML representation of the image with recognized text
        """
        output_dir = file_without_extension(image_path)
        #if not os.path.exists(output_dir):
        output_dir = self.binarize(image_path)
        #if not os.path.exists(join(output_dir, "0001")):
        self.segment(output_dir)

        prediction_method = self.__class__.TESSERACT \
            if use_tesseract_prediction else self.__class__.OCROPY

        self.predict(
            output_dir,
            method=prediction_method,
            lang=lang,
            force=force_prediction
        )

        html_file = self.perform_hocr(output_dir, label=prediction_method)
        text = self.hocr_to_text(html_file)

        return text

if __name__ == "__main__":
    assert len(sys.argv) >= 1, "Please provide a file"

    FILES_PATHS = sys.argv[1:]
    for FILE_PATH in FILES_PATHS:
        OCROPY_WRAPPER = Wocropy()
        TEXT = OCROPY_WRAPPER.scan(FILE_PATH, True, 'fra')

        print "------------------------"
        print "Here is the prediction :"
        print "------------------------"
        print TEXT

