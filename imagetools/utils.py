"""
Some utils used by Wocropy class
"""

import ntpath

def file_without_extension(filename, extension=None):
    """
    Extracts the filename without the extension

    Parameters
    ----------
    filename : string
        a filename/path
    extension : string | None
        file extension to remove or None.
        If None provided, every character after
        the last dot will be considered as
        an extension.
        """

    if extension is None:
        index = filename.rfind(".")
    else:
        index = filename.find(extension)

    return filename[0:index]


def path_leaf(path):
    """
    Returns last filename/dirname (leaf) from a complete path.
        
    Pre-conditions: path should correspond to a valid path

    Parameters
    ----------
        path : string
           File/dir path. It could be absolute or relative.
    """
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


