"""
This module contains image processing tools.
For now, it's only Wocropy, a wrapper of Ocropy that uses also tesseract in its transactions
"""
from .wocropy import Wocropy
